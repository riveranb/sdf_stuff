using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

public enum ColorChannel : int
{
    _RED = 0,
    _GREEN,
    _BLUE,
    _ALPHA,
}

public class RGBAMergeTextureWin : EditorWindow
{
    #region variables
    private ObjectField textureR = null;
    private ObjectField textureG = null;
    private ObjectField textureB = null;
    private ObjectField textureA = null;
    private EnumField channelR = null;
    private EnumField channelG = null;
    private EnumField channelB = null;
    private EnumField channelA = null;
    private Texture2D[] sourceTextures = new Texture2D[4];
    #endregion

    [MenuItem("SDF/RGBA Merge Texture Window")]
    public static void ShowExample()
    {
        RGBAMergeTextureWin wnd = GetWindow<RGBAMergeTextureWin>();
        wnd.titleContent = new GUIContent("Merge R/G/B/A Textures");
        wnd.Show();
    }

    public void CreateGUI()
    {
        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        // Import UXML
        var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/RGBAMergeTextureWin.uxml");
        visualTree.Instantiate();
        visualTree.CloneTree(root);

        var grabRoot = root.Q<VisualElement>("grab_red");
        textureR = grabRoot.Q<ObjectField>();
        textureR.label = "Channel Red";
        textureR.objectType = typeof(Texture2D);
        channelR = grabRoot.Q<EnumField>();
        channelR.Init(ColorChannel._ALPHA);

        grabRoot = root.Q<VisualElement>("grab_green");
        textureG = grabRoot.Q<ObjectField>();
        textureG.label = "Channel Green";
        textureG.objectType = typeof(Texture2D);
        channelG = grabRoot.Q<EnumField>();
        channelG.Init(ColorChannel._ALPHA);

        grabRoot = root.Q<VisualElement>("grab_blue");
        textureB = grabRoot.Q<ObjectField>();
        textureB.label = "Channel Blue";
        textureB.objectType = typeof(Texture2D);
        channelB = grabRoot.Q<EnumField>();
        channelB.Init(ColorChannel._ALPHA);

        grabRoot = root.Q<VisualElement>("grab_alpha");
        textureA = grabRoot.Q<ObjectField>();
        textureA.label = "Channel Alpha";
        textureA.objectType = typeof(Texture2D);
        channelA = grabRoot.Q<EnumField>();
        channelA.Init(ColorChannel._ALPHA);

        var button = root.Q<Button>();
        button.clicked += ClickGenerate;
    }

    private void ClickGenerate()
    {
        Texture2D sourceTemplate = null;
        sourceTextures[0] = (Texture2D)textureR.value;
        sourceTemplate = sourceTemplate ? sourceTemplate : sourceTextures[0];
        sourceTextures[1] = (Texture2D)textureG.value;
        sourceTemplate = sourceTemplate ? sourceTemplate : sourceTextures[1];
        sourceTextures[2] = (Texture2D)textureB.value;
        sourceTemplate = sourceTemplate ? sourceTemplate : sourceTextures[2];
        sourceTextures[3] = (Texture2D)textureA.value;
        sourceTemplate = sourceTemplate ? sourceTemplate : sourceTextures[3];
        if (!sourceTemplate)
        {
            return;
        }
        var destPath = EditorUtility.SaveFilePanel("Merged Image", "", "", "png");
        if (string.IsNullOrEmpty(destPath))
        {
            return;
        }

        TextureImporter[] importers = new TextureImporter[4];
        bool[] cachedReadable = new bool[4];
        TextureImporterCompression[] cachedCompressions = new TextureImporterCompression[4];
        for (int i = 0; i < 4; ++i)
        {
            if (sourceTextures[i] == null)
            {
                continue;
            }

            importers[i] = (TextureImporter)AssetImporter.GetAtPath(
                AssetDatabase.GetAssetPath(sourceTextures[i]));
            cachedReadable[i] = importers[i].isReadable;
            cachedCompressions[i] = importers[i].textureCompression;
            importers[i].isReadable = true;
            importers[i].textureCompression = TextureImporterCompression.Uncompressed;
            importers[i].SaveAndReimport();
        }

        var destTexture = CreateTextureFrom(sourceTemplate);
        MergeSourceTextures(destTexture);
        SaveTexture(destTexture, destPath);

        for (int i=0; i<4; ++i)
        {
            if (importers[i] == null)
            {
                continue;
            }
            importers[i].isReadable = cachedReadable[i];
            importers[i].textureCompression = cachedCompressions[i];
            importers[i].SaveAndReimport();
        }
    }

    private Texture2D CreateTextureFrom(Texture2D source)
    {
        var texture = new Texture2D(source.width, source.height, TextureFormat.ARGB32, false);
        texture.hideFlags = HideFlags.HideAndDontSave;
        return texture;
    }

    private void MergeSourceTextures(Texture2D dest)
    {
        Color[][] srcPixels = new Color[4][];
        for (int i = 0; i < 4; ++i)
        {
            if (sourceTextures[i] == null)
            {
                continue;
            }

            srcPixels[i] = sourceTextures[i].GetPixels();
        }

        int[] channels = new int[4];
        channels[0] = (int)(ColorChannel)channelR.value;
        channels[1] = (int)(ColorChannel)channelG.value;
        channels[2] = (int)(ColorChannel)channelB.value;
        channels[3] = (int)(ColorChannel)channelA.value;
        Color[] resultPixels = new Color[dest.width * dest.height];
        int index = 0;
        for (int y = 0; y < dest.height; ++y)
        {
            for (int x = 0; x < dest.width; ++x)
            {
                resultPixels[index] = new Color(0, 0, 0, 0);
                if (srcPixels[0] != null && index < srcPixels[0].Length)
                {
                    resultPixels[index].r = srcPixels[0][index][channels[0]];
                }
                if (srcPixels[1] != null && index < srcPixels[1].Length)
                {
                    resultPixels[index].g = srcPixels[1][index][channels[1]];
                }
                if (srcPixels[2] != null && index < srcPixels[2].Length)
                {
                    resultPixels[index].b = srcPixels[2][index][channels[2]];
                }
                if (srcPixels[3] != null && index < srcPixels[3].Length)
                {
                    resultPixels[index].a = srcPixels[3][index][channels[3]];
                }
                index++;
            }
        }

        dest.SetPixels(resultPixels);
        dest.Apply();
    }

    private void SaveTexture(Texture2D result, string path)
    {
        File.WriteAllBytes(path, result.EncodeToPNG());
        AssetDatabase.Refresh();
    }
}