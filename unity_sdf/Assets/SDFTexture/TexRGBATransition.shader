// ref: https://www.shadertoy.com/view/Nl2czt
// ref: https://www.shadertoy.com/view/XsfGRn

Shader "Unlit/SDF/Texture.RGBA Transition"
{
    Properties
    {
        _SDFTex("SDF Texture", 2D) = "black" {}
        _BackgroundColor("Background Color", Color) = (0, 0, 0, 0)
        _SDFColorR("SDF Color - Red Channel", Color) = (1, 0, 0, 1)
        _SDFColorG("SDF Color - Green Channel", Color) = (0, 1, 0, 1)
        _SDFColorB("SDF Color - Blue Channel", Color) = (0, 0, 1, 1)
        _SDFColorA("SDF Color - Alpha Channel", Color) = (0.8, 0.8, 0.8, 1)
        _FieldValve("Field Valve", Range(0.05, 0.95)) = 1
        _FieldBlurry("Field Edge Blurriness", Range(0.001, 0.3)) = 0.02
        _TransitionPeriod("Transition Period", Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Tranparent" "Queue"="Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            #define _fPI 3.1415926

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _SDFTex;
            half4 _SDFTex_ST;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _SDFTex);
                return o;
            }

            fixed4 _BackgroundColor;
            fixed4 _SDFColorR;
            fixed4 _SDFColorG;
            fixed4 _SDFColorB;
            fixed4 _SDFColorA;
            fixed _FieldBlurry;
            fixed _FieldValve;
            fixed _TransitionPeriod;

            fixed4 frag(v2f i) : SV_Target
            {
                float2 sdf_uv = i.uv;
                sdf_uv -= 0.5;

                float period = abs(frac(_Time.y / _TransitionPeriod + 0.5) - 0.5);
                int channel = floor(_Time.y / _TransitionPeriod) % 4;
                float threshold = max(period * 1.333 - 0.1, 0);

                // background color
                fixed4 background_color = _BackgroundColor;
                background_color.b -= abs(sdf_uv.y * 0.25);
                float radius = length(sdf_uv);
                background_color *= 1.0 - min(abs(radius * 0.3), 0.25);

                float sdf_circle = step(radius, threshold);

                // SDF texture sampling
                sdf_uv += 0.5;
                float4 distances = tex2D(_SDFTex, sdf_uv);
                float distance = min(sdf_circle, distances[channel]);

                fixed4 sdf_colors[4] = {
                    _SDFColorR,
                    _SDFColorG,
                    _SDFColorB,
                    _SDFColorA,
                };
                // signed distance field
                float sdf_field = _FieldValve - distance;
                // left less color impact for interior
                float valve = smoothstep(_FieldBlurry, -_FieldBlurry * 0.5, sdf_field);
                // sharpen shadow
                float sharpen = clamp(1.0 - radius / sdf_field, 0.0, 1.0);
                // final color composition (from outside-background to inside-heart)
                fixed4 final_color = lerp(background_color, sdf_colors[channel] * sharpen, valve);

                return final_color;
            }
            ENDCG
        }
    }
}
