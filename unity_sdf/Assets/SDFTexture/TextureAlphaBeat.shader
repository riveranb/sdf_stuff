// ref: https://www.shadertoy.com/view/Nl2czt
// ref: https://www.shadertoy.com/view/XsfGRn

Shader "Unlit/SDF/Texture.Alpha Beat"
{
    Properties
    {
        _SDFTex("SDF Texture", 2D) = "white" {}
        _BackgroundColor("Background Color", Color) = (0, 0, 0, 0)
        _BeatPeriod("Beat Period", Range(0.5, 3)) = 1
        _BeatAmplitude("Beat Amplitude", Range(0.05, 1)) = 0.5
        _SDFColor("SDF Color", Color) = (1.0, 0.6666, 0.3, 1)
        _FieldValve("Field Valve", Range(0.05, 0.95)) = 1
        _FieldBlurry("Field Edge Blurriness", Range(0.001, 0.3)) = 0.02
    }
    SubShader
    {
        Tags { "RenderType"="Tranparent" "Queue"="Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            #define _fPI 3.1415926

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _SDFTex;
            half4 _SDFTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _SDFTex);
                return o;
            }

            fixed4 _BackgroundColor;
            fixed _BeatPeriod;
            fixed _BeatAmplitude;
            fixed _FieldBlurry;
            fixed _FieldValve;
            fixed4 _SDFColor;

            fixed4 frag(v2f i) : SV_Target
            {
                float2 sdf_uv = i.uv;
                sdf_uv -= 0.5;

                // background color
                fixed4 background_color = _BackgroundColor;
                background_color.b -= abs(sdf_uv.y * 0.25);
                float radius = length(sdf_uv);
                background_color *= 1.0 - min(abs(radius * 0.3), 0.25);

                // animating beating over position
                float time_factor = fmod(_Time.y, _BeatPeriod) / _BeatPeriod;
                float sined_factor = pow(time_factor, 0.15) * 0.5 + 0.5;
                sined_factor = 1 +
                    sined_factor * _BeatAmplitude * sin(time_factor * _fPI * 4 - sdf_uv.y * 0.5) *
                    exp(-time_factor * 4);

                // SDF texture sampling
                sdf_uv *= float2(0.5, 1.5) + sined_factor * float2(0.5, -0.5);
                radius = length(sdf_uv);
                sdf_uv += 0.5;
                float distance = tex2D(_SDFTex, sdf_uv).a;

                // signed distance field
                float sdf_field = _FieldValve - distance;
                // left less color impact for interior
                float valve = smoothstep(_FieldBlurry, -_FieldBlurry * 0.5, sdf_field);
                // sharpen shadow
                float sharpen = clamp(1.0 - radius / sdf_field, 0.0, 1.0);
                // final color composition (from outside-background to inside-heart)
                fixed4 final_color = lerp(background_color, _SDFColor * sharpen, valve);

                return final_color;
            }
            ENDCG
        }
    }
}
