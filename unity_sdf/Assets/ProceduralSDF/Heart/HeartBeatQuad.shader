// ref: https://www.shadertoy.com/view/Nl2czt
// ref: https://www.shadertoy.com/view/XsfGRn

Shader "Unlit/SDF/HeartBeat Quad"
{
    Properties
    {
        _BackgroundColor("Background Color", Color) = (1, 1, 0.8, 1)
        _BeatPeriod("Beat Period", Range(0.5, 3)) = 1
        _BeatAmplitude("Beat Amplitude", Range(0.05, 1)) = 0.5
        _HeartColor("Heart Color", Color) = (1.0, 0.6666, 0.3, 1)
        _HeartDilate("Heart SDF Dilation", Float) = 1
        _HeartBlurry("Heart Edge Blurriness", Range(0.001, 0.3)) = 0.02
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            #define _fPI 3.1415926

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 _BackgroundColor;
            fixed _HeartDilate;
            fixed _BeatPeriod;
            fixed _BeatAmplitude;
            fixed _HeartBlurry;
            fixed4 _HeartColor;

            fixed4 frag(v2f i) : SV_Target
            {
                float2 sdf_pos = i.uv;
                sdf_pos.xy -= 0.5;
                sdf_pos.y -= 0.1;
                sdf_pos /= _HeartDilate;

                // background color
                fixed4 background_color = _BackgroundColor;
                background_color.b -= abs(sdf_pos.y * 0.25);
                float radius = length(sdf_pos);
                background_color *= 1.0 - min(abs(radius * 0.3), 0.25);

                // animating beating over position
                float time_factor = fmod(_Time.y, _BeatPeriod) / _BeatPeriod;
                float sined_factor = pow(time_factor, 0.15) * 0.5 + 0.5;
                sined_factor = 1 +
                    sined_factor * _BeatAmplitude * sin(time_factor * _fPI * 4 - sdf_pos.y * 0.5) *
                    exp(-time_factor * 4);

                // heart
                sdf_pos *= float2(0.5, 1.5) + sined_factor * float2(0.5, -0.5);
                radius = length(sdf_pos);
                float amp = abs(atan2(sdf_pos.x, sdf_pos.y) / _fPI);
                // TODO: how does this work?
                float heart_dist = (13 * amp - 22 * amp * amp + 10 * amp * amp * amp) /
                    (6 - 5 * amp);

                // colored
                float lighten_heart = 1.0;
                lighten_heart = 1 + sdf_pos.x;
                // sharpen shadow
                lighten_heart *= 0.5 + 0.5 * pow(1.0 - clamp(radius / heart_dist, 0.0, 1.0), 0.133);
                fixed4 heart_color = _HeartColor * lighten_heart;

                // signed distance field
                float heart_field = radius - heart_dist;
                heart_field *= _HeartDilate;
                float heart_valve = smoothstep(_HeartBlurry, -_HeartBlurry, heart_field);

                // final color composition (from outside-background to inside-heart)
                fixed4 final_color = lerp(background_color, heart_color, heart_valve);

                return final_color;
            }
            ENDCG
        }
    }
}
