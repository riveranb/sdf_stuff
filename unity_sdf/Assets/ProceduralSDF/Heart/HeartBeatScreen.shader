// ref: https://www.shadertoy.com/view/Nl2czt
// ref: https://www.shadertoy.com/view/XsfGRn

Shader "Image Effect/SDF/HeartBeat Screen"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _BackgroundColor("Background Color", Color) = (1, 1, 0.8, 1)
        _BeatPeriod("Beat Period", Range(0.5, 3)) = 1
        _BeatAmplitude("Beat Amplitude", Range(0.05, 1)) = 0.5
        _HeartColor("Heart Color", Color) = (1.0, 0.6666, 0.3, 1)
        _HeartDilate("Heart SDF Dilation", Float) = 0
        _HeartBlurry ("Heart Edge Blurriness", Range(0.001, 0.3)) = 0.02
        _LineSpacing ("Distance Line Spacing", Range(0.05, 1)) = 0.2
        _LineThickness ("Distance Line Thickness", Range (0.00005, 0.02)) = 0.001
        [Toggle(SDF_RAMPCOLORED_ON)] _SDFRampColored("SDF Ramp Colored", Float) = 0
        _SDFRampTex("SDF Ramp Texture", 2D) = "white" {}
        _SDFRampOffsetUnit("SDF Ramp Coloring Start Offset", Range(0, 2)) = 1
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma shader_feature_local SDF_RAMPCOLORED_ON

            #include "UnityCG.cginc"

            #define _fPI 3.1415926

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 screen : TEXCOORD0;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.screen = ComputeScreenPos(o.vertex);
                return o;
            }

            fixed _BeatPeriod;
            fixed _BeatAmplitude;
            fixed4 _BackgroundColor;
            fixed4 _HeartColor;
            fixed _HeartDilate;
            fixed _HeartBlurry;
            float _LineSpacing;
            float _LineThickness;

            sampler2D _MainTex;
#if SDF_RAMPCOLORED_ON
            sampler2D _SDFRampTex;
            half4 _SDFRampTex_TexelSize;
            fixed _SDFRampOffsetUnit;
#endif

            fixed4 frag(v2f i) : SV_Target
            {
                float2 screen_pos = i.screen.xy / i.screen.w * _ScreenParams.xy;
                float2 sdf_pos = (screen_pos + screen_pos - _ScreenParams.xy) / 
                    min(_ScreenParams.x, _ScreenParams.y);
                sdf_pos /= _HeartDilate;
                sdf_pos.y -= 0.3;

                // background color
                float3 background_color = _BackgroundColor;
                background_color.b -= abs(sdf_pos.y * 0.2);
                float radius = length(sdf_pos);
                background_color *= 1.0 - 0.3333 * radius;

                // animating beating over position
                float time_factor = fmod(_Time.y, _BeatPeriod) / _BeatPeriod;
                float sined_factor = pow(time_factor, 0.15) * 0.5 + 0.5;
                sined_factor = 1 +
                    sined_factor * _BeatAmplitude * sin(time_factor * _fPI * 4 - sdf_pos.y * 0.5) *
                    exp(-time_factor * 4);

                // heart
                sdf_pos *= float2(0.5, 1.5) + sined_factor * float2(0.5, -0.5);
                radius = length(sdf_pos);
                float amp = abs(atan2(sdf_pos.x, sdf_pos.y) / _fPI);
                // TODO: how does this work?
                float heart_dist = (13 * amp - 22 * amp * amp + 10 * amp * amp * amp) /
                    (6 - 5 * amp);

                // colored
                float lighten_heart = 1.0;
                lighten_heart = 1 + sdf_pos.x;
                lighten_heart *= 1.0 - 0.25 * radius;
                // sharpen shadow
                lighten_heart *= 0.5 + 0.5 * pow(1.0 - clamp(radius / heart_dist, 0.0, 1.0), 0.133);
                fixed3 heart_color = _HeartColor * lighten_heart;

                // distance field lines
                float heart_field = radius - heart_dist;
                heart_field *= _HeartDilate;
                float dgradient = fwidth(heart_field);
                float distance_coord = heart_field / _LineSpacing;
                distance_coord -= _Time.y; // animate moving
                float line_sdf = abs(frac(distance_coord + 0.5) - 0.5) * _LineSpacing;
                float lines = smoothstep(_LineThickness - dgradient, _LineThickness + dgradient, line_sdf);

                float heart_valve = smoothstep(_HeartBlurry, -_HeartBlurry, heart_field);
                lines = max(lines, heart_valve); // skip lines inside heart

#if SDF_RAMPCOLORED_ON
                distance_coord *= _SDFRampTex_TexelSize.x;
                fixed3 ramp = tex2D(_SDFRampTex, fixed2(distance_coord, 0)).rgb;
                float ramp_valve = smoothstep(_HeartBlurry, -_HeartBlurry, 
                    heart_field - _LineSpacing * _SDFRampOffsetUnit);
                background_color = lerp (background_color * ramp, background_color, ramp_valve);
#endif

                // final color composition (from outside-background to inside-heart)
                fixed3 final_color = lerp(background_color * lines, heart_color, heart_valve);

                return fixed4(final_color, 1);
            }
            ENDCG
        }
    }
}
