# sdf_stuff

My learning lessons / works about signed distance field

## Procedural
* Hearbeat screen post processing

![Hearbeat screen](https://gitlab.com/riveranb/sdf_stuff/-/raw/main/demo_snapshots/202207_unity_screen_postprocess_heartbeat.gif)

* Hearbeat cube

![Hearbeat geometry](https://gitlab.com/riveranb/sdf_stuff/-/raw/main/demo_snapshots/202207_unity_quad_heartbeat.gif)

## SDF Texture
* SDF texture RGBA channels morphing animation

![SDF texture morph](https://gitlab.com/riveranb/sdf_stuff/-/raw/main/demo_snapshots/202207_unity_SDFTexture_morph.gif)
